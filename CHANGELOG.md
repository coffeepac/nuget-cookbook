nuget cookbook CHANGELOG
====================
This file is used to list changes made in each version of the nuget cookbook.

1.0.0
-----
- [@bhamilton] - Initial release of nuget
