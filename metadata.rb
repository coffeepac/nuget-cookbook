name              'nuget'
maintainer        'DraftKings Inc.'
maintainer_email  'developers@draftkings.com'
license           'Apache 2.0'
description       'Installs/Configures nuget'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version           '1.0.1'

supports 'windows', '>= 6.1'

depends 'chocolatey'